package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

var (
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
	conf          *config
)

type config struct {
	PSQL_USER string
	PSQL_PASS string
	PSQL_PORT string
	PORT      string
}

func LoadConfig() *config {
	err := godotenv.Load(".env")
	if err != nil {
		log.Printf("Unable to load .env")
	}
	config := config{}
	config.PSQL_USER = os.Getenv("PSQL_USER")
	config.PSQL_PASS = os.Getenv("PSQL_PASS")
	config.PSQL_PORT = os.Getenv("PSQL_PORT")
	config.PORT = os.Getenv("PORT")

	return &config
}

func initLog() {
	InfoLogger = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarningLogger = log.New(os.Stderr, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)

	conf = LoadConfig()
}

var db *sql.DB

func main() {
	initLog()

	http.HandleFunc("/signin", Signin)
	http.HandleFunc("/signup", Signup)

	initDB()

	InfoLogger.Println("Listen and Serve :" + conf.PORT)
	ErrorLogger.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", conf.PORT), nil))
}

func initDB() {
	var err error
	// Connect to the postgres db
	//you might have to change the connection string to add your database credentials
	connStr := fmt.Sprintf("user=%s password=%s port=%s sslmode=disable", conf.PSQL_USER, conf.PSQL_PASS, conf.PSQL_PORT)
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		ErrorLogger.Fatal(err)
	}
	InfoLogger.Println("Signed into DB.")
}

// Create a struct that models the structure of a user, both in the request body, and in the DB
type Credentials struct {
	Password string `json:"password", db:"password"`
	Username string `json:"username", db:"username"`
}

func Signup(w http.ResponseWriter, r *http.Request) {
	InfoLogger.Println("Received signup request.")

	// Parse and decode the request body into a new `Credentials` instance
	creds := &Credentials{}
	InfoLogger.Println(r.Body)
	err := json.NewDecoder(r.Body).Decode(creds)
	if err != nil {
		WarningLogger.Println(err)
		// If there is something wrong with the request body, return a 400 status
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// Salt and hash the password using the bcrypt algorithm
	// The second argument is the cost of hashing, which we arbitrarily set as 8 (this value can be more or less, depending on the computing power you wish to utilize)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(creds.Password), 8)
	if err != nil {
		WarningLogger.Println(err)
		// If there is something wrong with the request body, return a 400 status
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Next, insert the username, along with the hashed password into the database
	if _, err = db.Query("insert into users values ($1, $2)", creds.Username, string(hashedPassword)); err != nil {
		WarningLogger.Println(err)
		// If there is any issue with inserting into the database, return a 500 error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// We reach this point if the credentials we correctly stored in the database, and the default status of 200 is sent back
}

func Signin(w http.ResponseWriter, r *http.Request) {
	InfoLogger.Println("Received signin request.")

	// Parse and decode the request body into a new `Credentials` instance
	creds := &Credentials{}
	err := json.NewDecoder(r.Body).Decode(creds)
	if err != nil {
		WarningLogger.Println(err)
		// If there is something wrong with the request body, return a 400 status
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// Get the existing entry present in the database for the given username
	result := db.QueryRow("select password from users where username=$1", creds.Username)
	if err != nil {
		WarningLogger.Println(err)
		// If there is an issue with the database, return a 500 error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// We create another instance of `Credentials` to store the credentials we get from the database
	storedCreds := &Credentials{}
	// Store the obtained password in `storedCreds`
	err = result.Scan(&storedCreds.Password)
	if err != nil {
		WarningLogger.Println(err)
		// If an entry with the username does not exist, send an "Unauthorized"(401) status
		if err == sql.ErrNoRows {
			WarningLogger.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		// If the error is of any other type, send a 500 status
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Compare the stored hashed password, with the hashed version of the password that was received
	if err = bcrypt.CompareHashAndPassword([]byte(storedCreds.Password), []byte(creds.Password)); err != nil {
		WarningLogger.Println(err)
		// If the two passwords don't match, return a 401 status
		w.WriteHeader(http.StatusUnauthorized)
	}

	// If we reach this point, that means the users password was correct, and that they are authorized
	// The default 200 status is sent
}
