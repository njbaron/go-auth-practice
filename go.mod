module gitlab.com/njbaron/go-auth-practice

go 1.16

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	gorm.io/driver/postgres v1.1.0 // indirect
	gorm.io/gorm v1.21.12 // indirect
)
